/**
 * Date: 12th May 2016
 * Header: pipes; @Author: Victor Huberta
 * ``````````````````````````````````````````````````
 * Define all pipes-related functions and constants.
 * ``````````````````````````````````````````````````
 *
 */

#ifndef PIPES_H
#define PIPES_H

#define LINK_READ 0
#define LINK_WRITE 1
#define LINK_PIPE_NAME "link_pipe"

/**
 * Pipe permissions are READ and WRITE.
 */
#define PIPE_PERMISSION 0666

/**
 * Map a READ file descriptor to STDIN and close the other end.
 */
#define MAP_IN_AND_CLOSE(pipefd) map_to_stdin(pipefd[0]); close(pipefd[1])

/**
 * Map a WRITE file descriptor to STDOUT and close the other end.
 */
#define MAP_OUT_AND_CLOSE(pipefd) map_to_stdout(pipefd[1]); close(pipefd[0])

void create_pipe(int pipefd[2]);
void map_to_stdin(int readfd);
void map_to_stdout(int writefd);

/**
 * Open a named pipe to link each branch to another.
 */
int open_link_pipe(int mode);

#endif
