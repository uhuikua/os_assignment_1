/**
 * Date: 14th May 2016
 * Module: branches; @Author: Victor Huberta
 * ``````````````````````````````````````````
 * This module handles how all branchings
 * occur. It also gives different jobs to
 * parent processes and child processes.
 * ``````````````````````````````````````````
 *
 */

#include "../main/main.h"
#include "../messages/messages.h"
#include "../pipes/pipes.h"
#include "branches.h"

/**
 * Function: branch
 *
 * args:
 * index -> a process' index in process list.
 * link_mode -> a branch's link type
 * (LINK_READ or LINK_WRITE).
 * ````````````````````````````````````````````
 * Create a new branch of processes by first
 * forking a child process. The forking occurs
 * recursively until index exceeds the length
 * of branch (for BRANCH A) or the length of
 * both branches (for BRANCH B).
 *
 * All processes are connected by unnamed pipes
 * and branches are connected by a named pipe.
 * Every process then proceed to call
 * wait_and_handle_messages.
 * ````````````````````````````````````````````
 * returns: void.
 *
 */
void branch(int index, int link_mode) {
    int pipefd[2];

    ++index;
    if (link_mode == LINK_WRITE && index > BRANCH_A_LEN) return;
    if (link_mode == LINK_READ && index > (BRANCH_A_LEN + BRANCH_B_LEN)) return;

    create_pipe(pipefd);

    switch (fork()) {
        case 0:
            do_child_job(index, link_mode, pipefd);
            _exit(EXIT_SUCCESS);
        case -1:
            perror("fork");
            exit(EXIT_FAILURE);
        default:
            do_parent_job(link_mode, pipefd);
    }
}

void do_child_job(int index, int link_mode, int pipefd[2]) {
    if (link_mode == LINK_WRITE) {
        /* for branch A */
        MAP_IN_AND_CLOSE(pipefd);
        if (index == BRANCH_A_LEN) {
            int linkfd = open_link_pipe(O_WRONLY);
            map_to_stdout(linkfd);
        }
    } else if (link_mode == LINK_READ) {
        /* for branch B */
        MAP_OUT_AND_CLOSE(pipefd);
        if (index == (BRANCH_A_LEN + BRANCH_B_LEN)) {
            int linkfd = open_link_pipe(O_RDONLY);
            map_to_stdin(linkfd);
        }
    }
    branch(index, link_mode);
    wait_and_handle_messages(index);
}

void do_parent_job(int link_mode, int pipefd[2]) {
    if (link_mode == LINK_WRITE) {
        /* for branch A */
        MAP_OUT_AND_CLOSE(pipefd);
    }
    else if (link_mode == LINK_READ) {
        /* for branch B */
        MAP_IN_AND_CLOSE(pipefd);
    }
}
