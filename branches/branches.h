/**
 * Date: 14th May 2016
 * Header: branches; @Author: Victor Huberta
 * ``````````````````````````````````````````
 * Define length of both branches and the
 * total number of processes. It defines
 * both parent and child's jobs after being
 * forked.
 * ``````````````````````````````````````````
 *
 */

#ifndef BRANCHES_H
#define BRANCHES_H

/**
 * Both BRANCH_A_LEN and BRANCH_B_LEN can be increased
 * or decreased according to requirements.
 */
#define BRANCH_A_LEN 2
#define BRANCH_B_LEN 1

/**
 * Fork child processes recursively until length
 * of branch is reached.
 */
void branch(int index, int link_mode);

/**
 * Parents & childs have to map their STDIN and
 * STDOUT to proper pipe ends.
 */
void do_child_job(int index, int link_mode, int pipefd[2]);
void do_parent_job(int link_mode, int pipefd[2]);

#endif
